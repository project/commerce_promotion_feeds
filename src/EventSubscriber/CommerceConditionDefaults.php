<?php

namespace Drupal\commerce_promotion_feeds\EventSubscriber;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_promotion_feeds\Event\CommerceConditionNormalizeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Handles normalization defaults for built-in Commerce Promotion offers.
 */
class CommerceConditionDefaults implements EventSubscriberInterface {

  /**
   * Currency Formatter Service.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * Constructs the commerce condition event subscriber.
   *
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   Currency Formatter Service.
   */
  public function __construct(CurrencyFormatterInterface $currency_formatter) {
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_product_feeds_promotion.promotion_condition_normalize.order_total_price' => ['orderTotalPrice'],
    ];
  }

  /**
   * Serializes order total into feed minimum purchase amount.
   *
   * @param \Drupal\commerce_promotion_feeds\Event\CommerceConditionNormalizeEvent $event
   *   The condition event.
   */
  public function orderTotalPrice(CommerceConditionNormalizeEvent $event) {
    $configuration = $event->getCondition()->getConfiguration();
    // This only works for minimum order totals.
    if ($configuration['operator'] !== '>') {
      return;
    }

    $amount = Price::fromArray($event->getCondition()->getConfiguration()['amount']);
    $event->setAttribute('g:minimum_purchase_amount', sprintf(
      '%s %s',
      $this->currencyFormatter->format(
        $amount->getNumber(), $amount->getCurrencyCode(),
        [
          'use_grouping' => FALSE,
          'currency_display' => 'none',
        ]
      ),
      $amount->getCurrencyCode()));
  }

}
