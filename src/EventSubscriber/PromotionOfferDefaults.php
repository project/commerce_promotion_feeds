<?php

namespace Drupal\commerce_promotion_feeds\EventSubscriber;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\ConditionGroup;
use Drupal\commerce\ConditionManagerInterface;
use Drupal\commerce\Plugin\Commerce\Condition\PurchasableEntityConditionInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\BuyXGetY;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderFixedAmountOff;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderItemFixedAmountOff;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderItemPercentageOff;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderPercentageOff;
use Drupal\commerce_promotion_feeds\Event\PromotionOfferNormalizeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Handles normalization defaults for built-in Commerce Promotion offers.
 */
class PromotionOfferDefaults implements EventSubscriberInterface {

  /**
   * Currency Formatter Service.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * Commerce Condition Manager.
   *
   * @var \Drupal\commerce\ConditionManagerInterface
   */
  protected $conditionManager;

  /**
   * Builds the subscriber object.
   *
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   Currency Formatter Service.
   * @param \Drupal\commerce\ConditionManagerInterface $condition_manager
   *   Commerce Condition Manager.
   */
  public function __construct(CurrencyFormatterInterface $currency_formatter, ConditionManagerInterface $condition_manager) {
    $this->currencyFormatter = $currency_formatter;
    $this->conditionManager = $condition_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_promotion_feeds.promotion_offer_normalize.order_buy_x_get_y' => ['buyXgetY'],
      'commerce_promotion_feeds.promotion_offer_normalize.order_fixed_amount_off' => ['fixedAmountOff'],
      'commerce_promotion_feeds.promotion_offer_normalize.order_item_fixed_amount_off' => ['fixedAmountOff'],
      'commerce_promotion_feeds.promotion_offer_normalize.order_item_percentage_off' => ['percentageOff'],
      'commerce_promotion_feeds.promotion_offer_normalize.order_percentage_off' => ['percentageOff'],
    ];
  }

  /**
   * Handles normalizing the BuyXGetY offer plugin.
   *
   * @param \Drupal\commerce_promotion_feeds\Event\PromotionOfferNormalizeEvent $event
   *   The event.
   */
  public function buyXgetY(PromotionOfferNormalizeEvent $event) {
    $offer = $event->getOffer();
    assert($offer instanceof BuyXGetY);

    // Set the buy quantity.
    $event->setAttribute('get_this_quantity_discounted', (int) $offer->getConfiguration()['buy_quantity']);

    // Could only be considered a valid free gift promotion if Y is 100% off.
    $y_percentage = Calculator::multiply($offer->getConfiguration()['offer_percentage'], '100');
    $get_conditions = $this->buildConditionGroup($offer->getConfiguration()['get_conditions']);
    $get_entity = $this->findSinglePurchasableEntity($get_conditions);
    if ($y_percentage === '100' && $get_entity) {
      // Set the free gift value.
      $event->setAttribute('g:free_gift_value', sprintf(
        '%s %s',
        $this->currencyFormatter->format(
          $get_entity->getPrice()->getNumber(), $get_entity->getPrice()
            ->getCurrencyCode(),
          [
            'use_grouping' => FALSE,
            'currency_display' => 'none',
          ]
        ),
        $get_entity->getPrice()->getCurrencyCode()));

      // Set the free gift item id.
      $event->setAttribute('g:free_gift_item_id', $get_entity->id());
    }
  }

  /**
   * Sets default promotion category data for fix amount off promotion plugins.
   *
   * @param \Drupal\commerce_promotion_feeds\Event\PromotionOfferNormalizeEvent $event
   *   The offer plugin normalizer event.
   */
  public function fixedAmountOff(PromotionOfferNormalizeEvent $event) {
    $offer = $event->getOffer();
    assert($offer instanceof OrderFixedAmountOff || $offer instanceof OrderItemFixedAmountOff);
    $amount = Price::fromArray($offer->getConfiguration()['amount']);
    $event->setAttribute('g:money_off_amount', sprintf(
      '%s %s',
      $this->currencyFormatter->format(
        $amount->getNumber(), $amount->getCurrencyCode(),
        [
          'use_grouping' => FALSE,
          'currency_display' => 'none',
        ]
      ),
      $amount->getCurrencyCode()));
  }

  /**
   * Sets default promotion category data for percentage off promotion plugins.
   *
   * @param \Drupal\commerce_promotion_feeds\Event\PromotionOfferNormalizeEvent $event
   *   The offer plugin normalizer event.
   */
  public function percentageOff(PromotionOfferNormalizeEvent $event) {
    $offer = $event->getOffer();
    assert($offer instanceof OrderPercentageOff || $offer instanceof OrderItemPercentageOff);
    $event->setAttribute('g:percent_off', Calculator::multiply($offer->getConfiguration()['percentage'], '100'));
  }

  /**
   * Finds the configured purchasable entity amongst the given conditions.
   *
   * @param \Drupal\commerce\ConditionGroup $conditions
   *   The condition group.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface|null
   *   The purchasable entity, or NULL if not found in the conditions.
   */
  protected function findSinglePurchasableEntity(ConditionGroup $conditions) {
    foreach ($conditions->getConditions() as $condition) {
      if ($condition instanceof PurchasableEntityConditionInterface) {
        $purchasable_entity_ids = $condition->getPurchasableEntityIds();
        if (count($purchasable_entity_ids) === 1) {
          $purchasable_entities = $condition->getPurchasableEntities();
          return reset($purchasable_entities);
        }
      }
    }

    return NULL;
  }

  /**
   * Builds a condition group for the given condition configuration.
   *
   * @param array $condition_configuration
   *   The condition configuration.
   *
   * @return \Drupal\commerce\ConditionGroup
   *   The condition group.
   */
  protected function buildConditionGroup(array $condition_configuration) {
    $conditions = [];
    foreach ($condition_configuration as $condition) {
      if (!empty($condition['plugin'])) {
        $conditions[] = $this->conditionManager->createInstance($condition['plugin'], $condition['configuration']);
      }
    }

    return new ConditionGroup($conditions, 'OR');
  }

}
