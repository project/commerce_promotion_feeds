<?php

namespace Drupal\commerce_promotion_feeds\Commands;

use Drupal\commerce_product_feeds\FeedGeneratorInterface;
use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacherInterface;
use Drupal\Core\File\FileSystemInterface;
use Drush\Commands\DrushCommands;
use Psr\Log\LoggerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class GenerationCommands extends DrushCommands {

  /**
   * Feed generator.
   *
   * @var \Drupal\commerce_product_feeds\FeedGeneratorInterface
   */
  protected $feedGenerator;

  /**
   * The Commerce Product Feeds channel logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $feedLogger;

  /**
   * File System.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Cacher.
   *
   * @var \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacherInterface
   */
  protected $cacher;

  /**
   * {@inheritDoc}
   */
  public function __construct(FeedGeneratorInterface $feedGenerator, FileSystemInterface $fileSystem, NormalizedValueCacherInterface $cacher, LoggerInterface $feedLogger) {
    parent::__construct();
    $this->feedGenerator = $feedGenerator;
    $this->feedLogger = $feedLogger;
    $this->fileSystem = $fileSystem;
    $this->cacher = $cacher;
  }

  /**
   * The product-feed:generate command.
   *
   * This command generates a product feed in a specified format and outputs
   * the result to a file. The underlying logic is contained in the generator
   * service.
   *
   * @param string $filename
   *   Filename to output generated feed.
   * @param array $options
   *   Array of cli switch options.
   *
   * @command promotion-feed:generate
   * @option string $format Format for the feed.
   */
  public function generate(string $filename, array $options = ['format' => 'xml_atom']): void {
    $context = ['options' => ['drush' => $options]];
    $output = $this->feedGenerator->generate($options['format'], $context);
    $this->fileSystem
      ->saveData($output, $filename, FileSystemInterface::EXISTS_REPLACE);

    $context = [
      '@filename' => $filename,
      '@memory' => format_size(memory_get_peak_usage()),
    ];
    $this->feedLogger->notice("Created product feed @filename. Peak memory usage @memory.", $context);
  }

}
