<?php

namespace Drupal\commerce_promotion_feeds;

use Drupal\commerce_product_feeds\FeedGeneratorInterface;
use Drupal\commerce_product_feeds\Serializer\Serializer;
use Drupal\commerce_promotion_feeds\Event\PromotionSelectionEvent;
use Drupal\Component\Utility\Timer;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Generates the promotions feed.
 */
class FeedGenerator implements FeedGeneratorInterface {

  /**
   * The name of the timer used for tracking generation.
   *
   * @var string
   */
  private const TIMER_NAME = 'commerce_promotion_feeds';

  /**
   * Serializer.
   *
   * @var \Drupal\commerce_product_feeds\Serializer\Serializer
   */
  protected $serializer;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_product_feeds\Serializer\Serializer $serializer
   *   Serializer.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $accountSwitcher
   *   The account switcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(
    Serializer $serializer,
    EventDispatcherInterface $eventDispatcher,
    EntityTypeManagerInterface $entityTypeManager,
    AccountSwitcherInterface $accountSwitcher,
    ConfigFactoryInterface $configFactory,
    LoggerInterface $logger
  ) {
    $this->serializer = $serializer;
    $this->eventDispatcher = $eventDispatcher;
    $this->entityTypeManager = $entityTypeManager;
    $this->accountSwitcher = $accountSwitcher;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(string $format, array $context = [], ?string $title = NULL, ?string $link = NULL): string {
    Timer::start(static::TIMER_NAME);

    $promotions = $this->getPromotions($format, $context);

    $title = $title ?? $this->configFactory->get('system.site')->get('name');
    $link = $link
      ?? Url::fromRoute('<front>')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    $collection = new PromotionCollection($promotions, $title, $link);
    $serialized = $this->serializer->serialize($collection, $format, $context);
    Timer::stop(static::TIMER_NAME);

    $log_context = [
      '@count' => count($promotions),
      '@format' => $format,
      '@time' => Timer::read(static::TIMER_NAME),
    ];
    $this->logger->notice("Generated @format feed with @count promotions in @time ms.", $log_context);

    return $serialized;
  }

  /**
   * Loads promotions to serialize.
   *
   * @param string $format
   *   Serializer format.
   * @param array $context
   *   Serializer options array.
   *
   * @return string[]|int[]
   *   Returns an array of promotion ids to serialize.
   */
  protected function getPromotions(string $format, array $context) : array {
    $event_name = 'commerce_promotion_feeds.promotion_selection';

    /** @var \Drupal\commerce_promotion\PromotionStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('commerce_promotion');
    if (!$this->eventDispatcher->hasListeners($event_name)) {
      $query = $storage->getQuery();
      $query->accessCheck(TRUE);

      // Only load enabled promotions.
      $query->condition('status', TRUE);

      // Do not load historical promotions.
      $date = new DrupalDateTime('now');
      $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $query->condition('end_date', $formatted, '>=');

      // Sane defaults here.
      return $query->execute();
    }

    $event = new PromotionSelectionEvent($format, $context);
    $this->eventDispatcher->dispatch($event, $event_name);
    return $event->getPromotionIds();
  }

}
