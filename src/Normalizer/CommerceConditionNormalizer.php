<?php

namespace Drupal\commerce_promotion_feeds\Normalizer;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface;
use Drupal\commerce_product_feeds\Normalizer\CommerceProductFeedsNormalizerBase;
use Drupal\commerce_promotion_feeds\Event\CommerceConditionNormalizeEvent;
use Drupal\Component\Assertion\Inspector;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Normalizes Commerce Conditions to be merged with promotion.
 */
class CommerceConditionNormalizer extends CommerceProductFeedsNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ConditionInterface::class;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs the normalizer.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher) {
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Normalizes Commerce Condition.
   *
   * @param mixed $object
   *   The Commerce Condition instance.
   * @param string $format
   *   The serialization format.
   * @param array $context
   *   Serializer array of options.
   *
   * @return array|\ArrayObject|bool|float|int|string|void|null
   *   Returns a serializable value to be merged with the promotion.
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $this->assertNormalizerObject($object);
    assert($object instanceof ConditionInterface);
    $value = [];
    $event = new CommerceConditionNormalizeEvent($object, $this->serializer, $value, $context);
    $this->eventDispatcher->dispatch($event, 'commerce_product_feeds_promotion.promotion_condition_normalize.' . $object->getPluginId());
    $value = $event->getValue();
    Inspector::assertAllStrings($value);
    return $value;
  }

}
