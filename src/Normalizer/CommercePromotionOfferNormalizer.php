<?php

namespace Drupal\commerce_promotion_feeds\Normalizer;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_product_feeds\Normalizer\CommerceProductFeedsNormalizerBase;
use Drupal\commerce_promotion_feeds\Event\PromotionOfferNormalizeEvent;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface;
use Drupal\Component\Assertion\Inspector;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Normalizes the promotion offer plugin instance to be merged with promotion.
 */
class CommercePromotionOfferNormalizer extends CommerceProductFeedsNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = PromotionOfferInterface::class;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Commerce Currency Formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * Constructs the normalizer.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   Commerce currency formatter service.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher, CurrencyFormatterInterface $currency_formatter) {
    $this->eventDispatcher = $eventDispatcher;
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * Normalizes the promotion offer plugin.
   *
   * @param mixed $object
   *   The offer plugin instance.
   * @param string $format
   *   Serialization format.
   * @param array $context
   *   Array of serialization options.
   *
   * @return array|\ArrayObject|bool|float|int|object|string|null
   *   Returns a serializable value of the offer plugin instance.
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $event_name = 'commerce_promotion_feeds.promotion_offer_normalize.' . $object->getPluginId();
    $this->assertNormalizerObject($object);
    assert($object instanceof PromotionOfferInterface);
    $value = [];
    $event = new PromotionOfferNormalizeEvent($object, $this->serializer, $value, $context);
    $event = $this->eventDispatcher->dispatch($event, $event_name);
    $value = $event->getValue();
    Inspector::assertAllStrings($value);
    return $value;
  }

}
