<?php

namespace Drupal\commerce_promotion_feeds\Normalizer;

use Drupal\commerce_price\CurrencyFormatter;
use Drupal\commerce_product_feeds\Event\EntityIdEvent;
use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher;
use Drupal\commerce_product_feeds\Normalizer\CommerceProductFeedsCachingNormalizerBase;
use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\commerce_promotion_feeds\Event\ApplicableProductsSelectionEvent;
use Drupal\commerce_promotion_feeds\Event\PromotionNormalizeEvent;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion_feeds\Event\ProductEntityTypeSelectionEvent;
use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Normalizes a promotion entity for the feed.
 */
class CommercePromotionNormalizer extends CommerceProductFeedsCachingNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = PromotionInterface::class;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Currency formatter.
   *
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $currencyFormatter;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(NormalizedValueCacher $cacher, EventDispatcherInterface $eventDispatcher, CurrencyFormatter $currencyFormatter, DateFormatterInterface $dateFormatter) {
    parent::__construct($cacher);
    $this->eventDispatcher = $eventDispatcher;
    $this->currencyFormatter = $currencyFormatter;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNormalization($object, $format, $context): CacheableNormalization {
    $this->assertNormalizerObject($object);
    /** @var \Drupal\commerce_promotion\Entity\PromotionInterface $object */

    $start_date = $object->getStartDate()->format('c');
    if ($date = $object->getEndDate()) {
      $end_date = $date->format('c');
    }
    else {
      $now = new DrupalDateTime('now');
      $end_date = $now->format('c');
    }

    // Generate the id for the entity object.
    $id_event_name = 'commerce_product_feeds.entity_id.' . $object->getEntityTypeId();
    $id_event = new EntityIdEvent($object, $context);
    if ($this->eventDispatcher->hasListeners($id_event_name)) {
      $this->eventDispatcher->dispatch($id_event, $id_event_name);
    }

    $value = [
      'g:promotion_id' => $id_event->getId(),
      'g:long_title' => $object->getDisplayName(),
      'g:description' => $object->getDescription(),
      // @todo Handle coupon codes.
      'g:offer_type' => $object->requiresCoupon() ? 'generic_code' : 'no_code',
      'g:promotion_effective_dates' => $start_date . '/' . $end_date,
      'g:redemption_channel' => [
        'online',
      ],
      'g:product_applicability' => 'specific_products',
    ];

    // Normalize the offer plugin.
    $value = array_merge($value, $this->serializer->normalize($object->getOffer(), $format, $context));

    foreach ($object->getConditions() as $condition) {
      $value = array_merge($value, $this->serializer->normalize($condition, $format, $context));
    }

    $normalize_event = new PromotionNormalizeEvent($object, $this->serializer, $value, $context);
    $this->eventDispatcher->dispatch($normalize_event, 'commerce_promotion_feeds.promotion_normalize');
    $normalize_event->addCacheableDependency($object);
    $value = $normalize_event->getValue();

    // @todo Filter this better.
    // Filter for products.
    foreach ($this->getProductFilterEntityTypeIds() as $entity_type_id) {
      $event_name = 'commerce_promotion_feeds.applicable_products_selection.' . $entity_type_id;
      if ($this->eventDispatcher->hasListeners($event_name)) {
        $selection_event = new ApplicableProductsSelectionEvent($object, $entity_type_id, $context);
        $this->eventDispatcher->dispatch($selection_event, $event_name);
        foreach ($selection_event->getEntities() as $purchasable_entity) {
          // Generate the id for the purchasable entity.
          $purchasable_entity_id_event_name = 'commerce_product_feeds.entity_id.' . $purchasable_entity->getEntityTypeId();
          $purchasable_entity_id_event = new EntityIdEvent($purchasable_entity, $context);
          if ($this->eventDispatcher->hasListeners($purchasable_entity_id_event_name)) {
            $this->eventDispatcher->dispatch($purchasable_entity_id_event, $purchasable_entity_id_event_name);
          }
          $value['g:item_id'][] = $purchasable_entity_id_event->getId();
        }
      }
    }

    Inspector::assertAllStrings($value);

    return (new CacheableNormalization($normalize_event, [$value]))->omitIfEmpty()
      ->addCacheTags([$object->getEntityTypeId() . '_feeds_normalization']);
  }

  /**
   * Gets list of purchasable entity types to select promotion applicability.
   *
   * @return string[]
   *   Returns a string of entity type ids.
   */
  public function getProductFilterEntityTypeIds() {
    $event = new ProductEntityTypeSelectionEvent();
    $this->eventDispatcher->dispatch($event, 'commerce_promotion_feeds.product_entity_type_selection');
    return $event->getEntityTypeIds();
  }

}
