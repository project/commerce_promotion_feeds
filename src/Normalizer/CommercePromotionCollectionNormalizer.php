<?php

namespace Drupal\commerce_promotion_feeds\Normalizer;

use Drupal\commerce_product_feeds\Normalizer\CommerceProductFeedsNormalizerBase;
use Drupal\commerce_product_feeds\Normalizer\Value\AggregatedNormalization;
use Drupal\commerce_promotion_feeds\PromotionCollection;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Normalizer for collection of promotion entities.
 */
class CommercePromotionCollectionNormalizer extends CommerceProductFeedsNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = PromotionCollection::class;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs the normalizer.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   Drupal Date Time formatter service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Drupal Time Service.
   */
  public function __construct(DateFormatterInterface $dateFormatter, TimeInterface $time) {
    $this->dateFormatter = $dateFormatter;
    $this->time = $time;
  }

  /**
   * {@inheritDoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $this->assertNormalizerObject($object);
    /** @var \Drupal\commerce_promotion_feeds\PromotionCollection $object */
    $values = [];
    foreach ($object->getIterator() as $promotion) {
      $values[] = $this->serializer->normalize($promotion, $format, $context);
    }

    $normalization = AggregatedNormalization::aggregatedMerged($values)->getNormalization();

    return array_merge(
      [
        'title' => $object->getTitle(),
        'id' => $object->getLink(),
        'link' => ['@rel' => 'self', '@href' => $object->getLink()],
        'updated' => $this->dateFormatter
          ->format($this->time->getRequestTime(), 'custom', \DateTimeInterface::ATOM),
        'generator' => 'Drupal Commerce',
      ],
      array_filter(['entry' => $normalization])
    );
  }

}
