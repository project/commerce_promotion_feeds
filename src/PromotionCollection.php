<?php

namespace Drupal\commerce_promotion_feeds;

use Drupal\commerce_promotion\Entity\Promotion;

/**
 * Iterator class for handling serialization of multiple promotions.
 */
class PromotionCollection implements \IteratorAggregate, \Countable {

  /**
   * Commerce promotion IDs to normalize into a feed.
   *
   * @var string[]
   */
  protected $data;

  /**
   * Title for XML feed.
   *
   * @var string|null
   */
  protected $title;

  /**
   * Link for XML feed.
   *
   * @var string|null
   */
  protected $link;

  /**
   * Instantiates a ProductCollection object.
   *
   * @param string[] $data
   *   The commerce product IDs for the collection.
   * @param string|null $title
   *   The title of the XML element.
   * @param string|null $link
   *   The link for the XML element.
   */
  public function __construct(array $data, ?string $title = NULL, ?string $link = NULL) {
    $this->data = array_values($data);
    $this->title = $title;
    $this->link = $link;
  }

  /**
   * Returns an iterator for entities.
   *
   * @return \Traversable
   *   Generator.
   */
  public function getIterator() {
    foreach ($this->data as $id) {
      yield Promotion::load($id);
    }
  }

  /**
   * Returns the number of promotions.
   *
   * @return int
   *   The number of promotions.
   */
  public function count() {
    return count($this->data);
  }

  /**
   * Getter for title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->title ?? '';
  }

  /**
   * Getter for link.
   *
   * @return string
   *   The link.
   */
  public function getLink(): string {
    return $this->link ?? '';
  }

}
