<?php

namespace Drupal\commerce_promotion_feeds\Event;

use Drupal\Component\Assertion\Inspector;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event for collecting promotion entities to serialize.
 */
class PromotionSelectionEvent extends Event {

  /**
   * Serialization format.
   *
   * @var string
   */
  protected $format;

  /**
   * Array of serialization options.
   *
   * @var array
   */
  protected $context;

  /**
   * Promotion entity ids to serialize.
   *
   * @var string[]|int[]
   */
  protected array $promotionIds = [];

  /**
   * Builds the promotion selection event.
   *
   * @param string $format
   *   Serialization format.
   * @param array $context
   *   Array of serialization options.
   */
  public function __construct(string $format, array $context) {
    $this->format = $format;
    $this->context = $context;
  }

  /**
   * Gets the serialization format.
   *
   * @return string
   *   The format.
   */
  public function getFormat() : string {
    return $this->format;
  }

  /**
   * Gets the serialization context options.
   *
   * @return array
   *   Array of serializer options.
   */
  public function getContext() : array {
    return $this->context;
  }

  /**
   * Gets the promotion entities to serialize.
   *
   * @return string[]|int[]
   *   Returns an array of promotion entity ids to serialize.
   */
  public function getPromotionIds() : array {
    return $this->promotionIds;
  }

  /**
   * Sets the promotions to serialize.
   *
   * @param string[]|int[] $promotion_ids
   *   Array of promotion entity ids.
   */
  public function setPromotions(array $promotion_ids) : void {
    Inspector::assertAllStringable($promotion_ids);
    $this->promotionIds = $promotion_ids;
  }

}
