<?php

declare(strict_types=1);

namespace Drupal\commerce_promotion_feeds\Event;

use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface;
use Drupal\Component\EventDispatcher\Event;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Event to normalize promotion offer plugins to be merged into promotion.
 */
class PromotionOfferNormalizeEvent extends Event {

  /**
   * The normalization context.
   *
   * @var array
   */
  protected $context;

  /**
   * The offer plugin instance.
   *
   * @var \Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface
   */
  protected $offer;

  /**
   * Serializer on the calling normalizer; e.g. for field values.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Normalized value.
   *
   * @var array
   */
  protected $value;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface $offer
   *   The offer.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer.
   * @param array $value
   *   The serialized value thus far.
   * @param array $context
   *   (optional) The normalization context.
   */
  public function __construct(PromotionOfferInterface $offer, SerializerInterface $serializer, array $value, array $context = []) {
    $this->offer = $offer;
    $this->serializer = $serializer;
    $this->value = $value;
    $this->context = $context;
  }

  /**
   * Gets the normalization context array.
   *
   * @return array
   *   The normalization context.
   */
  public function getContext(): array {
    return $this->context;
  }

  /**
   * Gets the offer plugin instance.
   *
   * @return \Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface
   *   The plugin instance.
   */
  public function getOffer() : PromotionOfferInterface {
    return $this->offer;
  }

  /**
   * Get the serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface
   *   Returns the serializer service.
   */
  public function getSerializer(): SerializerInterface {
    return $this->serializer;
  }

  /**
   * Get the normalized value.
   *
   * @return array
   *   Returns the normalized value as an array.
   */
  public function getValue(): array {
    return $this->value;
  }

  /**
   * Set the normalized value.
   *
   * @param array $value
   *   The value array.
   */
  public function setValue(array $value): void {
    $this->value = $value;
  }

  /**
   * Sets an attribute to the promotion data.
   *
   * @param string $attribute
   *   The attribute string.
   * @param mixed $value
   *   The serializable value of the attribute.
   */
  public function setAttribute(string $attribute, mixed $value) : void {
    $this->value[$attribute] = $value;
  }

  /**
   * Removes an attribute from the promotion data.
   *
   * @param string $attribute
   *   The attribute to remove.
   */
  public function unsetAttribute(string $attribute) : void {
    if (isset($this->value[$attribute])) {
      unset($this->value[$attribute]);
    }
  }

}
