<?php

declare(strict_types=1);

namespace Drupal\commerce_promotion_feeds\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Event to facilitate normalizing of a promotion entity object.
 */
class PromotionNormalizeEvent extends Event implements RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * The normalization context.
   *
   * @var array
   */
  protected $context;

  /**
   * The promotion entity.
   *
   * @var \Drupal\commerce_promotion\Entity\PromotionInterface
   */
  protected $promotion;

  /**
   * Serializer on the calling normalizer; e.g. for field values.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Normalized value.
   *
   * @var array
   */
  protected $value;

  /**
   * Builds the event.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion entity.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer.
   * @param array $value
   *   The serialized value thus far.
   * @param array $context
   *   (optional) The normalization context.
   */
  public function __construct(PromotionInterface $promotion, SerializerInterface $serializer, array $value, array $context = []) {
    $this->promotion = $promotion;
    $this->serializer = $serializer;
    $this->value = $value;
    $this->context = $context;
  }

  /**
   * Gets the normalization context array.
   *
   * @return array
   *   The normalization context.
   */
  public function getContext(): array {
    return $this->context;
  }

  /**
   * Gets the promotion entity.
   *
   * @return \Drupal\commerce_promotion\Entity\PromotionInterface
   *   The promotion entity.
   */
  public function getPromotion() : PromotionInterface {
    return $this->promotion;
  }

  /**
   * Get the serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface
   *   The serializer.
   */
  public function getSerializer(): SerializerInterface {
    return $this->serializer;
  }

  /**
   * Get the normalized value.
   *
   * @return array
   *   Returns an array of promotion feed data.
   */
  public function getValue(): array {
    return $this->value;
  }

  /**
   * Set the normalized value.
   *
   * @param array $value
   *   Sets the promotion feed data value.
   */
  public function setValue(array $value): void {
    $this->value = $value;
  }

  /**
   * Sets an attribute to the promotion data.
   *
   * @param string $attribute
   *   The attribute string.
   * @param mixed $value
   *   The serializable value of the attribute.
   */
  public function setAttribute(string $attribute, mixed $value) : void {
    $this->value[$attribute] = $value;
  }

  /**
   * Removes an attribute from the promotion data.
   *
   * @param string $attribute
   *   The attribute to remove.
   */
  public function unsetAttribute(string $attribute) : void {
    if (isset($this->value[$attribute])) {
      unset($this->value[$attribute]);
    }
  }

}
