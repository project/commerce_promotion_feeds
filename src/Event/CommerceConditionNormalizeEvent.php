<?php

namespace Drupal\commerce_promotion_feeds\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Event to normalize commerce conditions.
 */
class CommerceConditionNormalizeEvent extends Event {

  /**
   * The normalization context.
   *
   * @var array
   */
  protected $context;

  /**
   * The condition instance.
   *
   * @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface
   */
  protected $condition;

  /**
   * Serializer on the calling normalizer; e.g. for field values.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Normalized value.
   *
   * @var array
   */
  protected $value;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Condition\ConditionInterface $condition
   *   The condition instance.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer.
   * @param array $value
   *   The serialized value thus far.
   * @param array $context
   *   (optional) The normalization context.
   */
  public function __construct(ConditionInterface $condition, SerializerInterface $serializer, array $value, array $context = []) {
    $this->condition = $condition;
    $this->serializer = $serializer;
    $this->value = $value;
    $this->context = $context;
  }

  /**
   * Gets the condition instance.
   *
   * @return \Drupal\Core\Condition\ConditionInterface
   *   Returns the condition instance object.
   */
  public function getCondition() : ConditionInterface {
    return $this->condition;
  }

  /**
   * Gets the normalization context array.
   *
   * @return array
   *   The normalization context.
   */
  public function getContext(): array {
    return $this->context;
  }

  /**
   * Get the serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface
   *   Returns the serializer service.
   */
  public function getSerializer(): SerializerInterface {
    return $this->serializer;
  }

  /**
   * Get the normalized value.
   *
   * @return array
   *   Returns the normalized value as an array.
   */
  public function getValue(): array {
    return $this->value;
  }

  /**
   * Set the normalized value.
   *
   * @param array $value
   *   The value array.
   */
  public function setValue(array $value): void {
    $this->value = $value;
  }

  /**
   * Sets an attribute to the promotion data.
   *
   * @param string $attribute
   *   The attribute string.
   * @param mixed $value
   *   The serializable value of the attribute.
   */
  public function setAttribute(string $attribute, mixed $value) : void {
    $this->value[$attribute] = $value;
  }

  /**
   * Removes an attribute from the promotion data.
   *
   * @param string $attribute
   *   The attribute to remove.
   */
  public function unsetAttribute(string $attribute) : void {
    if (isset($this->value[$attribute])) {
      unset($this->value[$attribute]);
    }
  }

}
