<?php

namespace Drupal\commerce_promotion_feeds\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event to collect entity type ids for promotion product filtering.
 */
class ProductEntityTypeSelectionEvent extends Event {

  /**
   * List of entity type ids to collect promotion product filters for.
   *
   * @var string[]
   */
  protected array $entityTypeIds = [
    'commerce_product_variation',
  ];

  /**
   * Adds an entity type id to collect product filters for.
   *
   * @param string $entity_type_id
   *   The entity type id string.
   */
  public function addEntityTypeId(string $entity_type_id) : void {
    $this->entityTypeIds[] = $entity_type_id;
  }

  /**
   * Sets the entity type ids to filter products for.
   *
   * @param string[] $entity_type_ids
   *   Array of entity type ids.
   */
  public function setEntityTypeIds(array $entity_type_ids) : void {
    $this->entityTypeIds = $entity_type_ids;
  }

  /**
   * Gets the entity type ids.
   *
   * @return string[]
   *   Returns an array of entity type ids.
   */
  public function getEntityTypeIds() : array {
    return $this->entityTypeIds;
  }

}
