<?php

namespace Drupal\commerce_promotion_feeds\Event;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event to collect applicable purchasable entities for the promotion.
 */
class ApplicableProductsSelectionEvent extends Event {

  /**
   * The promotion to filter products against.
   *
   * @var \Drupal\commerce_promotion\Entity\PromotionInterface
   */
  protected $promotion;

  /**
   * The entity type id to filter for products on.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Normalizer context array of options.
   *
   * @var array
   */
  protected $context;

  /**
   * Array of purchasable entities.
   *
   * @var \Drupal\commerce\PurchasableEntityInterface[]
   */
  protected array $entities = [];

  /**
   * Builds the product filter event.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The entity type id to filter for products on.
   * @param string $entity_type_id
   *   The entity type id to filter for products on.
   * @param array $context
   *   Normalizer context array of options.
   */
  public function __construct(PromotionInterface $promotion, string $entity_type_id, array $context) {
    $this->promotion = $promotion;
    $this->entityTypeId = $entity_type_id;
    $this->context = $context;
  }

  /**
   * Gets the promotion entity to filter products against.
   *
   * @return \Drupal\commerce_promotion\Entity\PromotionInterface
   *   The entity.
   */
  public function getPromotion() : PromotionInterface {
    return $this->promotion;
  }

  /**
   * Gets the entity type id to base the product filtering on.
   *
   * @return string
   *   The entity type id string.
   */
  public function getEntityTypeId() : string {
    return $this->entityTypeId;
  }

  /**
   * Gets the normalization context options.
   *
   * @return array
   *   Returns an array of normalization options.
   */
  public function getContext() : array {
    return $this->context;
  }

  /**
   * Adds a purchasable entity to the applicable products.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   */
  public function addEntity(PurchasableEntityInterface $entity) : void {
    $this->entities[] = $entity;
  }

  /**
   * Sets the applicable entities.
   *
   * @param array $entities
   *   Array of applicable purchasable entities.
   */
  public function setEntities(array $entities) : void {
    $this->entities = $entities;
  }

  /**
   * Gets the applicable purchasable entities for the promotion.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface[]
   *   Array of entities.
   */
  public function getEntities() : array {
    return $this->entities;
  }

}
